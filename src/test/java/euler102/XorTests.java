package org.bitbucket.euler102;

import java.util.Arrays;
import java.util.Collection;
import static org.junit.Assert.*;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * Test suite for encoding a string
 */
@RunWith( Parameterized.class )
public class XorTests {

  /**
   *  The collection of tests as an Array of String x String x byte[]
   */
  /* *INDENT-OFF* */
  @Parameters
  public static Collection<Object[]> data() {
    // testcases will de displayed as test[0], test[1] and so on
    return Arrays.asList(new Object[][] {
        { "A",  "*"    , new byte[] {107} },
        { "k",  "*"    , new byte[] {65} },
        { "Here it is",  "*"    , new byte[] { 98, 79, 88, 79, 10, 67, 94, 10, 67, 89}  },
    });
  }
  /* *INDENT-ON* */

  // Test parameters
  // @link{https://github.com/junit-team/junit4/wiki/Parameterized-tests}
  private String stringToEncode;
  private byte[] encodedString;
  private String key;

  /**
   *  Constructor for the tests
   */
  public XorTests(String s,String k, byte[]  expectedResult) {
    key = k;
    stringToEncode = s;
    encodedString = expectedResult;
  }

  private Cypher c = new Cypher();
  /**
   * Run all the tests
   */
  @Test
  public void test()  {
    //  Should compare arrays of bytes
    //  TODO: tests fail but don't know why. Check how to compare arrays with Junit?
    assertArrayEquals(encodedString, c.cypher(stringToEncode, key));
  }
}
